#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "updater.h"
#include "sortproxymodel.h"
#include <sys/sysinfo.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    updateInterval = DEFAULT_UPDATE_INTERVAL;
    ui->setupUi(this);
    tmodel = new TableModel(this);
    this->setStaticData();

    QSortFilterProxyModel *proxyModel = new SortProxyModel(this);
    proxyModel->setSourceModel(tmodel);
    ui->tableView->setModel(proxyModel);
    ui->tableView->setSortingEnabled(true);
    tmodel->updateProcInfo();
    this->setDynamicData();

    QThread *workerThread = new QThread(this);
    Updater *upd = new Updater(this);
    connect(workerThread, SIGNAL(started()), upd, SLOT(doWork()));
    connect(upd, SIGNAL(waitingComplete()), this, SLOT(updateAll()));
    connect(this, SIGNAL(updatingDone()), upd, SLOT(doWork()));
    upd->moveToThread(workerThread);
    workerThread->start();
    QStringList seconds;
    for (int i = MIN_INTERVAL; i <= MAX_INTERVAL; i++)
        seconds << QString::number(i);
    ui->comboBox->addItems(seconds);
    ui->comboBox->setCurrentIndex(ui->comboBox->findText(QString::number(DEFAULT_UPDATE_INTERVAL)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::getUpdateInterval()
{
    return updateInterval;
}

void MainWindow::setStaticData()
{
    struct sysinfo myinfo;
    sysinfo(&myinfo);
    ui->lineEdit_tm->setText(QString::number(myinfo.totalram/1024) + " kB");
    ui->lineEdit_ts->setText(QString::number(myinfo.totalswap/1024) + " kB");
    int memory = sysconf(_SC_PAGE_SIZE);
    ui->lineEdit_ps->setText(QString::number(memory) + " bytes");
}

void MainWindow::setDynamicData()
{
    struct sysinfo myinfo;
    sysinfo(&myinfo);
    ui->lineEdit_fm->setText(QString::number(myinfo.freeram/1024) + " kB");
    ui->lineEdit_fs->setText(QString::number(myinfo.freeswap/1024) + " kB");
    ui->lineEdit_prcs->setText(QString::number(tmodel->rowCount()));
}

void MainWindow::updateAll()
{
    tmodel->updateProcInfo();
    this->setDynamicData();
    this->updatingDone();
}

void MainWindow::on_comboBox_currentIndexChanged(const QString &arg1)
{
    updateInterval = arg1.toDouble();
}
