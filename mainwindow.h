#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "tablemodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int getUpdateInterval();
    
private:
    static const int DEFAULT_UPDATE_INTERVAL = 4;
    static const int MIN_INTERVAL = 2;
    static const int MAX_INTERVAL = 6;

    Ui::MainWindow *ui;
    TableModel *tmodel;

    void setStaticData();
    void setDynamicData();
    int updateInterval;

public slots:
    void updateAll();

signals:
    void updatingDone();

private slots:
    void on_comboBox_currentIndexChanged(const QString &arg1);
};

#endif // MAINWINDOW_H
