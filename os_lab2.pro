#-------------------------------------------------
#
# Project created by QtCreator 2013-05-05T04:55:20
#
#-------------------------------------------------

QT       += core gui

TARGET = os_lab2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tablemodel.cpp \
    processview.cpp \
    updater.cpp \
    sortproxymodel.cpp

HEADERS  += mainwindow.h \
    tablemodel.h \
    processview.h \
    updater.h \
    sortproxymodel.h

FORMS    += mainwindow.ui
