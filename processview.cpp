#include "processview.h"

ProcessView::ProcessView()
{
    isExists = true;
    cpuUsageSet = false;
}

ProcessView::~ProcessView()
{
}

bool ProcessView::checkState()
{
    return isExists;
}

void ProcessView::setState(bool status)
{
    isExists = status;
}

int ProcessView::calculateCPUUsage(long timeNew, long userNew, long kernelNew)
{
    if (cpuUsageSet) {
        int userUtil = 100 * (userNew - userOld) / (timeNew - timeOld);
        int sysUtil = 100 * (kernelNew - kernelOld) / (timeNew - timeOld);
        return userUtil + sysUtil;
    }
    else return -1;
}

bool ProcessView::getCpuUsageStatus()
{
    return cpuUsageSet;
}

void ProcessView::setStartTime(long time, long user, long kernel)
{
    if (!cpuUsageSet) {
        timeOld = time;
        userOld = user;
        kernelOld = kernel;
        cpuUsageSet = true;
    }
}




