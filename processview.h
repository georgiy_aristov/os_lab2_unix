#ifndef PROCESSVIEW_H
#define PROCESSVIEW_H
#include <QtCore>
#include <QtGui>

class ProcessView
{
public:
    ProcessView();
    bool checkState();
    void setState(bool) ;
    int calculateCPUUsage(long timeNew, long userNew, long kernelNew);
    bool getCpuUsageStatus();
    void setStartTime(long time, long user, long kernel);
    ~ProcessView();
    QList<QStandardItem*> processData;

private:
    bool cpuUsageSet;
    bool isExists;
    long timeOld;
    long userOld;
    long kernelOld;
};

#endif // PROCESSVIEW_H
