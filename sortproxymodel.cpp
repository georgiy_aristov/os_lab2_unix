#include "sortproxymodel.h"

SortProxyModel::SortProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

bool SortProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QString leftData = sourceModel()->data(left).toString();
    QString rightData = sourceModel()->data(right).toString();

    QRegExp rx("(\\t*\\ +\\t*)");

    bool ok;
    double leftValue = leftData.split(rx).at(0).toDouble(&ok);
    double rightValue = rightData.split(rx).at(0).toDouble();
    if (ok)
        return leftValue < rightValue;
    return (QString::compare(leftData, rightData, Qt::CaseInsensitive) < 0);
}
