#include "tablemodel.h"
#include "processview.h"

TableModel::TableModel(QObject *parent) :
    QStandardItemModel(parent)
{
    initialize();
}

void TableModel::initialize()
{
    setHorizontalHeaderLabels(QStringList() << tr("PID")
                              << tr("Name") << tr("State") << tr("UID")
                              << tr("VSZ") << tr("RSS") << tr("CPU"));
}

void TableModel::updateProcInfo()
{
    QDir proc("/proc");
    proc.setFilter(QDir::Dirs);


    QStringList entryList = proc.entryList();

    QStringList keys = processes.keys();
    QStringListIterator iterator(keys);
    while (iterator.hasNext())
        processes[iterator.next()]->setState(false);

    for (int i = 0; i < entryList.count(); i++) {
        if (!entryList.at(i).at(0).isDigit())
            continue;

        bool isNew = false;
        const QString currentPID = entryList.at(i);
        ProcessView *currentProcess;

        if (!this->processes.contains(currentPID)) {
            currentProcess = new ProcessView();
            QStandardItem* pid = new QStandardItem(currentPID);
            pid->setFlags(pid->flags() ^ Qt::ItemIsEditable);
            currentProcess->processData << pid;

            processes.insert(currentPID, currentProcess);
            isNew = true;
        } else {
            currentProcess = processes[currentPID];
        }
        readStatus(*currentProcess, currentPID, isNew);
        readCpu(*currentProcess, currentPID);

        if (isNew)
            appendRow(currentProcess->processData);
        processes[currentPID]->setState(true);
    }

    iterator.toFront();
    while (iterator.hasNext()) {
        QString key = iterator.next();
        if (!processes[key]->checkState()) {
            QModelIndex ind = this->indexFromItem(processes[key]->processData.at(0));
            this->removeRow(ind.row());
            delete processes[key];
            processes.remove(key);
        }
    }
}

void TableModel::readStatus(ProcessView &process, QString pid, bool createNew)
{
    QStringList statusFieldsToRead;
    statusFieldsToRead << "Name" << "State" << "Uid" << "VmSize" << "VmRSS";
    QRegExp rx("(\\:\\t\\ +|\\:*\\t)");
    ProcessView *currentProcess = &process;
    QString currentPID = pid;

    QFile procStatus("/proc/" + pid + "/status");
    if (procStatus.open(QIODevice::ReadOnly)) {
        QTextStream status(&procStatus);
        bool memoryDataExists = false;
        do {
            QStringList currentLine = status.readLine().split(rx);
            if(statusFieldsToRead.contains(currentLine.at(0))) {
                if (createNew) {
                    QStandardItem *it = new QStandardItem(currentLine.at(1));
                    it->setFlags(it->flags() ^ Qt::ItemIsEditable);
                    currentProcess->processData << it;
                } else {
                    int index = statusFieldsToRead.indexOf(currentLine.at(0));
                    currentProcess->processData[index+1]->setText(currentLine.at(1));
                }
            }
            if(currentLine.at(0) == "VmSize")
                memoryDataExists = true;
        } while (!status.atEnd());
        if (!memoryDataExists) {
            QStandardItem *vsz = new QStandardItem("0 kB");
            vsz->setFlags(vsz->flags() ^ Qt::ItemIsEditable);
            QStandardItem *rss = new QStandardItem("0 kB");
            rss->setFlags(rss->flags() ^ Qt::ItemIsEditable);
            currentProcess->processData << vsz << rss;
        }
    }
    procStatus.close();
}

void TableModel::readCpu(ProcessView &process, QString pid)
{
    const int STAT_UTIME_POSITION = 13;
    const int STAT_STIME_POSITION = 14;
    const int CPU_USAGE_COLUMNNO = 6;

    ProcessView *currentProcess = &process;

    long time = 0;
    QRegExp rx("\\t*\\ +\\t*");
    QFile cpuTime("/proc/stat");
    if (cpuTime.open(QIODevice::ReadOnly)) {
        QTextStream cpu(&cpuTime);
        QStringList times = cpu.readLine().split(rx);
        cpuTime.close();
        for (int i = 1; i < times.count(); i++)
            time += times[i].toDouble();
    }

    QFile jiffies("/proc/" + pid + "/stat");
    if (jiffies.open(QIODevice::ReadOnly)) {
        QTextStream procStat(&jiffies);
        QStringList data = procStat.readLine().split(rx);
        jiffies.close();

        if (!currentProcess->getCpuUsageStatus()) {
            currentProcess->setStartTime(time, data[STAT_UTIME_POSITION].toDouble(), data[STAT_STIME_POSITION].toDouble());
            QStandardItem *cpu = new QStandardItem("0 %");
            cpu->setFlags(cpu->flags() ^ Qt::ItemIsEditable);
            currentProcess->processData << cpu;
        } else {
            int percentage = currentProcess->calculateCPUUsage(time, data[STAT_UTIME_POSITION].toDouble(), data[STAT_STIME_POSITION].toDouble());
            currentProcess->processData[CPU_USAGE_COLUMNNO]->setText(QString::number(percentage)+ " %");
        }
    }
}



