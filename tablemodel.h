#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QStandardItemModel>
#include <QtCore>
#include <QtGui>
#include "processview.h"


class TableModel : public QStandardItemModel
{
    Q_OBJECT
public:
    explicit TableModel(QObject *parent = 0);
    void updateProcInfo();

    
signals:
    
public slots:

private:
    QMap <QString, ProcessView*> processes;
    void initialize();
    void readStatus(ProcessView &process, QString pid, bool createNew);
    void readCpu(ProcessView &process, QString pid);
    
};

#endif // TABLEMODEL_H
