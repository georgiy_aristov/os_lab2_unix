#include "updater.h"

Updater::Updater(MainWindow *parent)
{
    mainParent = parent;
}

void Updater::doWork()
{
    struct timespec ts = { mainParent->getUpdateInterval(), 0 };
    nanosleep(&ts, NULL);
    waitingComplete();
}
