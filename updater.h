#ifndef UPDATER_H
#define UPDATER_H

#include <QtGui>
#include <QtCore>
#include "mainwindow.h"

class Updater : public QObject
{
    Q_OBJECT

public:
    Updater(MainWindow *parent);

public slots:
    void doWork();

signals:
    void waitingComplete();

private:
    MainWindow* mainParent;

};

#endif // UPDATER_H
